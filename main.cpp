#include <string>
#include <iostream>
#include <pqxx/pqxx>
#include "db.h"
#include "menu.h"

int main()
{
    DB *db;
    try {
        db =  new DB ("localhost", "adomas", "studentu", "");
    }
    catch(const std::exception &e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }
    if(db!=NULL){
        Menu menu(db);
        menu.doMenu();
    }

    delete db;

    return 0;
}

