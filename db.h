#ifndef DB_H
#define DB_H

#include <pqxx/pqxx>
#include <string>
#include <iostream>
#include <iomanip>

class DB
{
private:
    pqxx::connection *conn;
    pqxx::result exec(const std::string query);

public:
    DB(const std::string host, const std::string username, const std::string dbname, const std::string password);
    ~DB();
    pqxx::result SelectZaidejas();
    pqxx::result SelectAdresas();
    pqxx::result SelectKomanda();
    pqxx::result SelectMacas();
    pqxx::result InsertZaidejas(const std::string vardas, const int pasto_kodas, const std::string komanda);
    pqxx::result InsertKomanda(const std::string pavadinimas);
    pqxx::result InsertAdresas(const int pasto_kodas, const int pastato_numeris, const std::string gatve, const std::string gyvenviete, const std::string rajonas);
    pqxx::result InsertMacas(const std::string pirmoji_komanda, const std::string antroji_komanda, const int pirmosios_taskai, const int antrosios_taskai, const std::string data);
    pqxx::result DeleteAdresas(const int pasto_kodas);
    pqxx::result DeleteZaidejas(const std::string vardas);
    pqxx::result DeleteKomanda(const std::string pavadinimas);
    pqxx::result DeleteMacas(const std::string pirmoji_komanda, const std::string antroji_komanda, const std::string data);
    pqxx::result UpdateZaidejas(const std::string vardas, const int pasto_kodas, const std::string komanda);
    pqxx::result InsertKomandaZaidejas(const std::string pavadinimas, const std::string vardas, const int pasto_kodas);
    pqxx::result UpdateMacas(const std::string pirmoji_komanda, const std::string antroji_komanda, const std::string data, const int pirmosios_taskai, const int antrosios_taskai);
};

#endif // DB_H
