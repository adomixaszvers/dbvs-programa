#include "menu.h"

void Menu::doInsertZaidejas()
{
    std::string vardas;
    int pasto_kodas;
    std::string komanda;
    std::string input;

    std::cout << "Įveskite žaidejo duomenis." << std::endl;
    std::cout << "vardas: ";
    std::getline(std::cin, vardas);
    std::cout << "pašto kodas: ";
    std::getline(std::cin, input);
    std::stringstream myStrStream(input);
    myStrStream >> pasto_kodas;
    std::cout << "komandos pavadinimas: ";
    std::getline(std::cin, komanda);

    try {
        db->InsertZaidejas(vardas, pasto_kodas, komanda);
    }
    catch (pqxx::unique_violation &sqle) {
        std::cerr << "Žaidėjas " << vardas << " jau egzistuoja." << std::endl;
    }

    catch (pqxx::foreign_key_violation &sqle) {
        std::cerr << sqle.what() << std::endl;
        std::cerr << "Užregistruokite komandą arba įrašykite adresą, o tada bandykite iš naujo." << std::endl;
    }

    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }

}

void Menu::doUpdateZaidejas()
{
    std::string vardas, input, komanda;
    int pasto_kodas;


    std::cout << "Įveskite žaidejo, kurio duomenis norite keisti, vardą: ";
    std::getline(std::cin, vardas);

    std::cout << "Iveskite keičiamus duomenis."<< std::endl;
    std::cout << "pašto kodas: ";
    std::getline(std::cin, input);
    std::stringstream myStrStream(input);
    myStrStream >> pasto_kodas;
    std::cout << "komanda: ";
    std::getline(std::cin, komanda);

    try {
        db->UpdateZaidejas(vardas, pasto_kodas, komanda);
    }
    catch (pqxx::unique_violation &sqle) {
        std::cerr << "Žaidėjas " << vardas << " jau egzistuoja." << std::endl;
    }

    catch (pqxx::foreign_key_violation &sqle) {
        std::cerr << sqle.what() << std::endl;
        std::cerr << "Užregistruokite komandą arba įrašykite adresą (adreso įrašyti kol kas negalima), o tada bandykite iš naujo." << std::endl;
    }

    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }
}

void Menu::doDeleteZaidejas()
{
    std::string vardas;

    std::cout << "Įveskite zaidejo, kurį norite išregistruoti, vardą: ";
    std::getline(std::cin, vardas);

    try {
        db->DeleteZaidejas(vardas);
    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }
}

void Menu::doSelectZaidejas()
{
    std::cout << "Visų žaidėjų sąrašas" << std::endl;
    try {
        printResultTable(db->SelectZaidejas());
    } catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }
}

void Menu::doInsertKomanda()
{
    std::string komanda;
    std::cout << "Įveskite naujos komandos pavadinimą: ";
    std::getline(std::cin, komanda);

    try {
        db->InsertKomanda(komanda);
    }
    catch (pqxx::unique_violation &sqle) {
        std::cerr << "Komanda " << komanda << " jau egzistuoja." << std::endl;
    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }
}

void Menu::doDeleteKomanda()
{
    std::string komanda;

    std::cout << "Įveskite pavadinimą, kurią komandą norite ištrinti: ";
    std::getline(std::cin, komanda);

    try {
        db->DeleteKomanda(komanda);
    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }
}

void Menu::doSelectKomanda()
{
    std::cout << "Visų komandų sąrašas." << std::endl;

    try {
        printResultTable(db->SelectKomanda());
    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }
}

void Menu::doInsertMacas()
{
    std::string pirmoji_komanda, antroji_komanda, data, input;
    std::stringstream *myStringStream;
    int pirmosios_taskai, antrosios_taskai;

    std::cout << "Įveskite mačo duomenis." << std::endl;
    std::cout << "pirmosios komandos pavadinimas: ";
    std::getline(std::cin, pirmoji_komanda);
    std::cout << "antrosios komandos pavadinimas: ";
    std::getline(std::cin, antroji_komanda);
    std::cout << "pirmosios komandos taskai: ";
    std::getline(std::cin, input);
    myStringStream = new std::stringstream(input);
    *myStringStream >> pirmosios_taskai;
    delete myStringStream;
    std::cout << "antrosios komandos taskai: ";
    std::getline(std::cin, input);
    myStringStream = new std::stringstream(input);
    *myStringStream >> antrosios_taskai;
    delete myStringStream;
    std::cout << "mačo data formatu MMMM-MM-DD: ";
    std::getline(std::cin, data);

    try {
        db->InsertMacas(pirmoji_komanda, antroji_komanda, pirmosios_taskai, antrosios_taskai, data);
    }
    catch (pqxx::unique_violation &sqle) {
        std::cerr << "Toks mačas jau egzistuoja." << std::endl;
    }

    catch (pqxx::foreign_key_violation &sqle) {
        std::cerr << sqle.what() << std::endl;
        std::cerr << "Užregistruokite komandą (komandas), o tada bandykite iš naujo." << std::endl;
    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }
}

void Menu::doUpdateMacas()
{
    std::string pirmoji_komanda, antroji_komanda, data, input;
    std::stringstream *myStringStream;
    int pirmosios_taskai, antrosios_taskai;

    std::cout << "Įveskite mačo duomenis." << std::endl;
    std::cout << "pirmosios komandos pavadinimas: ";
    std::getline(std::cin, pirmoji_komanda);
    std::cout << "antrosios komandos pavadinimas: ";
    std::getline(std::cin, antroji_komanda);
    std::cout << "mačo data formatu MMMM-MM-DD: ";
    std::getline(std::cin, data);
    std::cout << "Šie duomenys bus pakeisti." << std::endl;
    std::cout << "pirmosios komandos taskai: ";
    std::getline(std::cin, input);
    myStringStream = new std::stringstream(input);
    *myStringStream >> pirmosios_taskai;
    delete myStringStream;
    std::cout << "antrosios komandos taskai: ";
    std::getline(std::cin, input);
    myStringStream = new std::stringstream(input);
    *myStringStream >> antrosios_taskai;
    delete myStringStream;

    try {
        db->UpdateMacas(pirmoji_komanda, antroji_komanda, data, pirmosios_taskai, antrosios_taskai);
    }
    catch (pqxx::unique_violation &sqle) {
        std::cerr << "Toks mačas jau egzistuoja." << std::endl;
    }

    catch (pqxx::foreign_key_violation &sqle) {
        std::cerr << sqle.what() << std::endl;
        std::cerr << "Užregistruokite komandą (komandas), o tada bandykite iš naujo." << std::endl;
    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }
}

void Menu::doSelectMacas()
{
    std::cout << "Visų mačų sąrašas." << std::endl;

    try {
        printResultTable(db->SelectMacas());
    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }
}

void Menu::doSelectAdresas()
{
    std::cout << "Visų adresų sąrašas." << std::endl;

    try {
        printResultTable(db->SelectAdresas());
    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }
}

void Menu::doInsertKomandaZaidejas()
{
    std::string vardas;
    int pasto_kodas;
    std::string komanda;
    std::string input;

    std::cout << "Įveskite žaidejo duomenis." << std::endl;
    std::cout << "vardas: ";
    std::getline(std::cin, vardas);
    std::cout << "pašto kodas: ";
    std::getline(std::cin, input);
    std::stringstream myStrStream(input);
    myStrStream >> pasto_kodas;
    std::cout << "komandos pavadinimas (bus kuriama ir tokia komanda): ";
    std::getline(std::cin, komanda);

    try {
        db->InsertKomandaZaidejas(komanda, vardas, pasto_kodas);
    }

    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }
}

Menu::Menu(DB * const db)
{
    this->db=db;
}

void Menu::printResultTable(const pqxx::result &result)
{
    std::cout << "+++++" << std::endl;
    std::cout << '|';
    for(pqxx::result::size_type i=0; i<result.columns(); i++) {
        std::cout << result.column_name(i) << '|';
    }
    std::cout << std::endl << "-----" << std::endl;

    for(pqxx::result::const_iterator row=result.begin();row!=result.end(); ++row) {
        std::cout << '|';
        for(pqxx::result::tuple::const_iterator field=row.begin(); field!=row.end(); ++field) {
            std::cout << field.c_str() << '|';
        }
        std::cout<< std::endl;
    }
    std::cout<<"+++++"<<std::endl;
}

void Menu::doMenu()
{
    bool exit = false;
    std::string input;
    std::stringstream *myStrStream;
    while(!exit) {
        std::cout << "1. Užregistruoti naują žaidėją;" << std::endl;
        std::cout << "2. Pakeisti žaidėjo duomenis;" << std::endl;
        std::cout << "3. Išregistruoti žaidėją;" << std::endl;
        std::cout << "4. Parodyti visų žaidėjų sąrašą" << std::endl;

        std::cout << "5. Užregistruoti naują komandą;" << std::endl;
        std::cout << "6. Išregistruoti komandą;" << std::endl;
        std::cout << "7. Parodyti visų komandų sąrašą;" << std::endl;

        std::cout << "8. Paskelbti naują mačą" << std::endl;
        std::cout << "9. Pakeisti mačo rezultatą;" << std::endl;
        std::cout << "10. Parodyti visų mačų sąrašą;" << std::endl;

        std::cout << "11. Parodyti visų adresų sąrašą;" << std::endl;

        std::cout << "12. Baigti." << std::endl;
        std::cout << "13. Užregistruoti naują komandą IR žaidėją;" << std::endl;
        std::cout << "Įveskite skaičių 1-13: ";
        int selection;
        std::getline(std::cin, input);
        myStrStream = new std::stringstream(input);
        *myStrStream >> selection;
        delete myStrStream;
        switch  (selection) {
        case 1:
            doInsertZaidejas();
            break;
        case 2:
            doUpdateZaidejas();
            break;
        case 3:
            doDeleteZaidejas();
            break;
        case 4:
            doSelectZaidejas();
            break;
        case 5:
            doInsertKomanda();
            break;
        case 6:
            doDeleteKomanda();
            break;
        case 7:
            doSelectKomanda();
            break;
        case 8:
            doInsertMacas();
            break;
        case 9:
            doUpdateMacas();
            break;
        case 10:
            doSelectMacas();
            break;
        case 11:
            doSelectAdresas();
            break;
        case 12:
            exit=true;
            break;

        case 13:
            doInsertKomandaZaidejas();
            break;
        }
    }

}
