#ifndef MENU_H
#define MENU_H

#include <pqxx/pqxx>
#include <iostream>
#include "db.h"

class Menu
{
private:
    DB *db;
    void doInsertZaidejas();
    void doUpdateZaidejas();
    void doDeleteZaidejas();
    void doSelectZaidejas();

    void doInsertKomanda();
    void doDeleteKomanda();
    void doSelectKomanda();

    void doInsertMacas();
    void doUpdateMacas();
    void doSelectMacas();

    void doSelectAdresas();

    void doInsertKomandaZaidejas();

public:
    Menu(DB * const db);
    void printResultTable(const pqxx::result &result);
    void doMenu();
};

#endif // MENU_H
