project(DBVS)
set(CMAKE_CXX_FLAGS "-lpqxx")
add_library(DB db.cpp)
add_library(Menu menu.cpp)
cmake_minimum_required(VERSION 2.8)
aux_source_directory(. SRC_LIST)
add_executable(${PROJECT_NAME} ${SRC_LIST})

