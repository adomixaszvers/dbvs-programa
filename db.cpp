#include "db.h"

DB::DB(const std::string host, const std::string username, const std::string dbname, const std::string password)
{
    std::string connstr("host=");
    //%s dbname=%s user=%s password=%s"
    //connstr % host % dbname % username % password;
    conn = new pqxx::connection(connstr+host+" dbname="+dbname+" user="+username+" password="+password);

    std::string sql = "INSERT INTO adja0844.zaidejas(vardas, pasto_kodas, komanda) values ($1, $2, $3)";
    conn->prepare("InsertZaidejas", sql);

    sql = "INSERT INTO adja0844.adresas(pasto_kodas, pastato_numeris, gatve, gyvenviete, rajonas) values ($1, $2, $3, $4, $5)";
    conn->prepare("InsertAdresas", sql);

    sql = "INSERT INTO adja0844.komanda values($1)";
    conn->prepare("InsertKomanda", sql);

    sql = "INSERT INTO adja0844.macas values($1, $2, $3, $4, $5)";
    conn->prepare("InsertMacas", sql);

    sql = "DELETE from adja0844.adresas WHERE pasto_kodas=$1";
    conn->prepare("DeleteAdresas", sql);

    sql = "DELETE from adja0844.zaidejas WHERE vardas=$1";
    conn->prepare("DeleteZaidejas", sql);

    sql =  "DELETE from adja0844.komanda WHERE pavadinimas=$1";
    conn->prepare("DeleteKomanda", sql);

    sql = "DELETE from adja0844.macas WHERE pirmoji_komanda=$1 and antroji_komanda=$2 and data=$3";
    conn->prepare("DeleteMacas", sql);

    sql = "UPDATE adja0844.zaidejas SET pasto_kodas=$1, komanda=$2 WHERE vardas=$3";
    conn->prepare("UpdateZaidejas", sql);

    sql = "UPDATE adja0844.macas SET pirmosios_taskai=$1, antrosios_taskai=$2 WHERE pirmoji_komanda=$3 AND antroji_komanda=$4 AND data=$5";
    conn->prepare("UpdateMacas", sql);
}

DB::~DB() {
    delete conn;
}

pqxx::result DB::exec(const std::string query) {
    try {
        pqxx::work work(*conn);
        pqxx::result result = work.exec(query);
        work.commit();
        return result;
    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }
}

pqxx::result DB::SelectZaidejas() {
    return exec("SELECT * FROM adja0844.zaidejas ORDER BY vardas ASC");
}

pqxx::result DB::SelectAdresas() {
    return exec("SELECT * FROM adja0844.adresas");
}

pqxx::result DB::SelectKomanda() {
    return exec(std::string("SELECT")+
                    " komanda.pavadinimas, komanda_baige_lygiosiomis.kiek AS lygiuju, komanda_pralaimejo.kiek AS pralaimeta, komanda_laimejo.kiek AS laimeta, komandos_nariu_skaicius.skaicius as nariu_skaicius"+
                " FROM"+
                    " adja0844.komanda"+
                ",adja0844.komanda_baige_lygiosiomis"+
                ",adja0844.komanda_pralaimejo"+
                ",adja0844.komanda_laimejo"+
                ",adja0844.komandos_nariu_skaicius"+
                " WHERE komanda.pavadinimas=komanda_baige_lygiosiomis.pavadinimas"+
                " AND komanda.pavadinimas=komanda_pralaimejo.pavadinimas"+
                " AND komanda.pavadinimas=komanda_laimejo.pavadinimas"+
                " AND komanda.pavadinimas=komandos_nariu_skaicius.pavadinimas"+
                //" GROUP BY komanda.pavadinimas"+
                " ORDER BY komanda.pavadinimas ASC");
}

pqxx::result DB::SelectMacas() {
    return exec("SELECT * from adja0844.macas ORDER BY data ASC");
}

pqxx::result DB::InsertZaidejas(const std::string vardas, const int pasto_kodas, const std::string komanda) {
    pqxx::work work(*conn);
    pqxx::result result = work.prepared("InsertZaidejas")(vardas)(pasto_kodas)(komanda).exec();
    work.commit();
    return result;
}

pqxx::result DB::InsertKomanda(const std::string pavadinimas) {
    pqxx::work work(*conn);
    pqxx::result result = work.prepared("InsertKomanda")(pavadinimas).exec();
    work.commit();
    return result;
}

pqxx::result DB::InsertAdresas(const int pasto_kodas, const int pastato_numeris, const std::string gatve, const std::string gyvenviete, const std::string rajonas)
{
    pqxx::work work(*conn);
    pqxx::result result = work.prepared("InsertAdresas")(pasto_kodas)(pastato_numeris)(gatve)(gyvenviete)(rajonas).exec();
    work.commit();
    return result;
}

pqxx::result DB::InsertMacas(const std::string pirmoji_komanda, const std::string antroji_komanda, const int pirmosios_taskai, const int antrosios_taskai, const std::string data)
{
    pqxx::work work(*conn);
    pqxx::result result = work.prepared("InsertMacas")(pirmoji_komanda)(antroji_komanda)(pirmosios_taskai)(antrosios_taskai)(data).exec();
    work.commit();
    return result;
}

pqxx::result DB::DeleteAdresas(const int pasto_kodas)
{
    pqxx::work work(*conn);
    pqxx::result result = work.prepared("DeleteAdresas")(pasto_kodas).exec();
    work.commit();
    return result;
}

pqxx::result DB::DeleteZaidejas(const std::string vardas)
{
    pqxx::work work(*conn);
    pqxx::result result = work.prepared("DeleteZaidejas")(vardas).exec();
    work.commit();
    return result;
}

pqxx::result DB::DeleteKomanda(const std::string pavadinimas)
{
    pqxx::work work(*conn);
    pqxx::result result = work.prepared("DeleteKomanda")(pavadinimas).exec();
    work.commit();
    return result;
}

pqxx::result DB::DeleteMacas(const std::string pirmoji_komanda, const std::string antroji_komanda, const std::string data)
{
    pqxx::work work(*conn);
    pqxx::result result = work.prepared("DeleteMacas")(pirmoji_komanda)(antroji_komanda)(data).exec();
    work.commit();
    return result;
}

pqxx::result DB::UpdateZaidejas(const std::string vardas, const int pasto_kodas, const std::string komanda)
{
    pqxx::work work(*conn);
    pqxx::result result = work.prepared("UpdateZaidejas")(pasto_kodas)(komanda)(vardas).exec();
    work.commit();
    return result;
}

pqxx::result DB::InsertKomandaZaidejas(const std::string pavadinimas, const std::string vardas, const int pasto_kodas)
{
    pqxx::work work(*conn);
    pqxx::result result = work.prepared("InsertKomanda")(pavadinimas).exec();
    work.prepared("InsertZaidejas")(vardas)(pasto_kodas)(pavadinimas).exec();
    work.commit();
    return result;
}

pqxx::result DB::UpdateMacas(const std::string pirmoji_komanda, const std::string antroji_komanda, const std::string data, const int pirmosios_taskai, const int antrosios_taskai)
{
    pqxx::work work(*conn);
    pqxx::result result = work.prepared("UpdateMacas")(pirmosios_taskai)(antrosios_taskai)(pirmoji_komanda)(antroji_komanda)(data).exec();
    work.commit();
    return result;
}
